package persadoproject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestAutomationFile {
	public String baseUrl ="https://www.amazon.com";
	String driverPath = "C:\\Users\\filik\\Downloads\\chromedriver_win32\\chromedriver.exe";
	public WebDriver driver;

	@BeforeTest
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver = new ChromeDriver();
		driver.get(baseUrl);
	}

	@BeforeClass
	public void Preconditions() throws InterruptedException {
		driver.findElement(By.linkText("Today's Deals")).click();
		//No action for about 2 seconds in order to let the browser load the current function
		Thread.sleep(2000);
		//1st listed product/category
		driver.findElement(By.linkText("See details")).click();
		Thread.sleep(2000);

		int i = 0;
		//for loop in list of elements with the same class name, in our case in the same item category
		for (WebElement element : driver.findElements(By.className("a-link-normal"))){

			i++;
			//3rd item if it is a category 
			if (i == 3) {
				element.click();
				break;
			}

		}
		Thread.sleep(2000);

		//Add product/item to cart 
		driver.findElement(By.id("add-to-cart-button")).click();
		Thread.sleep(2000);
		


	}

	@Test
	public void priceVerification() throws InterruptedException {

		//Get product price
		String productPrice = driver.findElement(By.id("attach-accessory-cart-subtotal")).getText();
		Thread.sleep(2000);

		//Go to basket
		driver.findElement(By.className("a-button-input")).click();
		Thread.sleep(2000);

		//Get product price on basket list
		String verifiedPrice = driver.findElement(By.id("sc-subtotal-amount-buybox")).getText();
		Thread.sleep(2000);

		//Price Verification
		if (productPrice.equalsIgnoreCase(verifiedPrice)) {
			System.out.println("Price verification successful!");
		}
		else {
			System.out.println("Price verification failed!");
		}
		Thread.sleep(2000);
	}

	@AfterTest
	public void terminateBrowser() {
		driver.close();
	}
}
